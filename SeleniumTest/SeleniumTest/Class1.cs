﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using OpenQA.Selenium;
//using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;

namespace SeleniumTest
{
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.IE;
    using System;
    [TestFixture]
    public class GmailTests
    {
        private const string DRIVER_PATH = @"C:\selenium_drivers";
        private IWebDriver driver;
        public GmailTests() { }
        [SetUp]
        public void LoadDriver()
        {
            /*
         * for chrome drivers: https://sites.google.com/a/chromium.org/chromedriver/
         * for ie driver http://selenium-release.storage.googleapis.com/2.44/IEDriverServer_x64_2.44.0.zip
         */
            Console.WriteLine("SetUp");
            driver = new ChromeDriver(DRIVER_PATH);
        }
        [Test]
        public void Login()
        {
            Console.WriteLine("Test");
            driver.Navigate().GoToUrl("http://gmail.com");
            driver.FindElement(By.Id("Email")).SendKeys("test");
            driver.FindElement(By.Id("Passwd")).SendKeys("test");
            driver.FindElement(By.Id("Passwd")).Submit();
            Assert.True(driver.Title.Contains("Inbox"));
        }
        [TearDown]
        public void UnloadDriver()
        {
            Console.WriteLine("TearDown");
            driver.Quit();
        }
    }
    [TestFixture, Description("Tests Google Search with String data")]
    public class GoogleTests
    {
        private const string DRIVER_PATH = @"C:\selenium_drivers";
        private IWebDriver driver;
        public GoogleTests() { }
        [SetUp]
        public void LoadDriver() { driver = new ChromeDriver(DRIVER_PATH); }
        [TestCase("Google")]   // searchString = Google
        [TestCase("Bing")]     // searchString = Bing
        public void Search(string searchString)
        {
            // execute Search twice with testdata: Google, Bing
            driver.Navigate().GoToUrl("http://google.com");
            driver.FindElement(By.Name("q")).SendKeys(searchString);
            driver.FindElement(By.Name("q")).Submit();
            Assert.True(driver.Title.Contains("Google"));
        }
        [TearDown]
        public void UnloadDriver() { driver.Quit(); }
    }
    public class RivneStudentsTests
    {
        private const string DRIVER_PATH = @"C:\selenium_drivers";
        private IWebDriver driver;
        public RivneStudentsTests() { }
        [SetUp]
        public void LoadDriver()
        {
            Console.WriteLine("SetUp");
            driver = new ChromeDriver(DRIVER_PATH);
        }
        [Test]
        public void Login()
        {
            driver.Navigate().GoToUrl("http://rivnestudents.com/");
            Console.WriteLine("Login to rivnestudents.com");
            try
            {
                // Find the text input element by its name
                IWebElement email = driver.FindElement(By.Id("Email"));
                IWebElement password = driver.FindElement(By.Id("Password"));
                //Make some pause, wait until browser load all data
                Thread.Sleep(1000);
                //Type login and pass
                email.SendKeys("sport@ymail.com");
                password.SendKeys("1111111");
                //Search login button
                driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
                //Wait untill all site login
                Thread.Sleep(3000);
                //Check for successfull authorization. If we see Global News Button - than check it's color (UI/UX test)
                IWebElement globalNews = driver.FindElement(By.Id("GlobalNewsButtonDown"));
                string c = globalNews.GetCssValue("color");
                Assert.AreEqual(c, "rgba(51, 147, 236, 1)");
                
            }
            catch (Exception e) // in other case - make screenshot and save it
            {
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                //ss.SaveAsFile("C:\\ScreenShot" + "\\" + "LoginSuccesfulorNot.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                throw e;
            }
        }
        [TearDown]
        public void UnloadDriver()
        {
            Console.WriteLine("Unload driver");
            driver.Quit();
        }
    }

    public class RivneStudentsTag
    {
        private const string DRIVER_PATH = @"C:\selenium_drivers";
        private IWebDriver driver;
        public RivneStudentsTag() { }
        [SetUp]
        public void LoadDriver()
        {
            Console.WriteLine("SetUp");
            driver = new ChromeDriver(DRIVER_PATH);
        }
        [Test]
        public void Tag()
        {
            driver.Navigate().GoToUrl("http://rivnestudents.com/");
            Console.WriteLine("Tags in rivnestudents.com");
            try
            {
                IWebElement email = driver.FindElement(By.Id("Email"));
                IWebElement password = driver.FindElement(By.Id("Password"));
                //Make some pause, wait until browser load all data
                Thread.Sleep(1000);
                //Type login and pass
                email.SendKeys("irinniada123@gmail.com");
                password.SendKeys("19081996");
                //Search login button
                driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
                Thread.Sleep(6000);

                // Find the element

                IWebElement search = driver.FindElement(By.CssSelector("input[placeholder=\"Ключові теги\"]"));
                driver.FindElement(By.CssSelector("input[placeholder=\"Ключові теги\"]")).Click();
                Thread.Sleep(1000);
                search.SendKeys("смішно\n");
                //Thread.Sleep(1000);
                //driver.FindElement(By.CssSelector("input[role=\"listbox\"]")).Click();
                //Make some pause, wait until browser load all data
                Thread.Sleep(5000);
                driver.FindElement(By.Id("FollowingHashtag")).Click();
                
                //IWebElement follow = driver.FindElement(By.Id("FollowingHashtag"));
                              
                Thread.Sleep(3000);
                //Check for successfull authorization. If we see Global News Button - than check it's color (UI/UX test)
                IWebElement followed = driver.FindElement(By.Id("FollowingHashtag"));
                string c = followed.GetCssValue("color");
                Assert.AreNotEqual(c, "rgba(255, 15, 0, 1)");
                
                                

            }
            catch (Exception e) // in other case - make screenshot and save it
            {
                Screenshot ss = ((ITakesScreenshot)driver).GetScreenshot();
                //ss.SaveAsFile("C:\\ScreenShot" + "\\" + "LoginSuccesfulorNot.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
                throw e;
            }
        }
        [TearDown]
        public void UnloadDriver()
        {
            Console.WriteLine("Unload driver");
            driver.Quit();
        }
    }
}